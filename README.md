# GimmeBadge
A Discord bot for getting Active Developer Badge with the /gimmebadge command.

## Obtaining the Badge

1. Clone this repo and run `pip install -r requirements.txt`.

2. Go to the [Discord Developers Portal](https://discord.com/developers) and click "Create Application".

3. Select your application, go to OAuth2 > Bot, check "Send Messages", copy the invite link, and paste it in your browser to select a server.

4. Under Bot, find your token (or click "Reset Token" if needed).

5. Paste the token into `bot.run("YOUR_BOT_TOKEN")` on line 7.

6. Run `gimmebadge.py`. If the bot is online in your server, refresh Discord (Ctrl+R or ⌘+R) and use `/gimmebadge`.