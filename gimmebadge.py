import discord

bot = discord.Bot()

@bot.slash_command(name="gimmebadge", description="Sends the link for the Active Developer Badge page!")
async def hello(ctx: discord.ApplicationContext):
    await ctx.respond("Okay! Check here in 24-48 hours: https://discord.com/developers/active-developer")

bot.run("YOUR_BOT_TOKEN")
